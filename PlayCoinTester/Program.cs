﻿using PlayCoinLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayCoinTester
{
    class Program
    {
        static void Main(string[] args)
        {
            PlayCoin playCoin = new PlayCoin();
            var task = Task.Run(async () =>
            {
                Console.WriteLine(await playCoin.SignGame("game", "12345"));
                Console.WriteLine(await playCoin.SignPlayer("tokyo", "12345"));
            //    Console.Write(await playCoin.set());

            });
                task.Wait();
            Console.WriteLine();
            Console.ReadKey();
        }
    }
}
